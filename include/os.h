#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#define KEY_LENGTH 32

int read_key_file(const char* filename,const unsigned char* key_buf);
char* exec_shell_command(const char* command);
