#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <linux/if.h>

#define VERSION_STRING "0.1.1"
#define DEFAULT_SERVER_IP "126.49.232.0"
#define DEFAULT_CLIENT_IP "14.88.66.66"
#define DEFAULT_PORT "1234"
#define DEFAULT_MTU 9000;

typedef struct Context_{
  int is_server;
  unsigned char key[32];
  unsigned int tun_fd; 
  const char* vpn_server_ipv4_or_name;
  const char* vpn_server_port;
  const char* tun_desired_name;
  const char* local_tun_ip;
  const char* remote_tun_ip;
  const char* local_tun_ip6;
  const char* remote_tun_ip6;
  const char* vpn_gateway_ip_desired;
  const char* default_interface_name;
  char if_name[IFNAMSIZ];
} Context;

static char* get_default_gateway_ip();
static char* get_default_interface_name();
void get_tun6_addresses(Context* context);
