#include "include/vpn.h"
#include "include/os.h"
#include <linux/if.h>
#include <stdio.h>
#include <sys/ioctl.h>



__attribute__((__noreturn__)) static void usage(void)
{
  puts("CVPN " VERSION_STRING
         " usage:\n"
         "\n"
         "cvpn\t\"server\"\n\t<key file>\n\t<vpn server ip or name>|\"auto\"\n\t<vpn "
         "server port>|\"auto\"\n\t<tun interface>|\"auto\"\n\t<local tun "
         "ip>|\"auto\"\n\t<remote tun ip>\"auto\"\n\t<external ip>|\"auto\""
         "\n\n"
         "cvpn\t\"client\"\n\t<key file>\n\t<vpn server ip or name>\n\t<vpn server "
         "port>|\"auto\"\n\t<tun interface>|\"auto\"\n\t<local tun "
         "ip>|\"auto\"\n\t<remote tun ip>|\"auto\"\n\t<gateway ip>|\"auto\"\n\n"
         "Example:\n\n[server]\n\tdd if=/dev/urandom of=vpn.key count=1 bs=32\t# create key\n"
         "\tbase64 < vpn.key\t\t# copy key as a string\n\tsudo ./cvpn server vpn.key\t# listen on "
         "443\n\n[client]\n\techo ohKD...W4= | base64 --decode > vpn.key\t# paste key\n"
         "\tsudo ./cvpn client vpn.key 34.216.127.34\n");
  exit(-1);
}

int main(int argc,char *argv[])
{
  if(argc < 3)
    usage();
  Context context;
  context.is_server = strcmp(argv[1], "server") == 0;
  if(read_key_file(argv[2], context.key)){
    return -2;
  }
  context.vpn_server_ipv4_or_name = (argc >= 4 && !context.is_server) ? argv[3] : NULL;
  if(!context.is_server && context.vpn_server_ipv4_or_name == NULL)
    usage();
  context.vpn_server_port = argc >= 5 ? argv[4] : DEFAULT_PORT;
  context.tun_desired_name = argc >= 6 ? argv[5] : NULL;
  context.local_tun_ip =  argc >= 7 ? (context.is_server ? DEFAULT_SERVER_IP : DEFAULT_CLIENT_IP)
                                      : argv[6];
  context.remote_tun_ip = argc >= 8 ? (context.is_server ? DEFAULT_CLIENT_IP : DEFAULT_SERVER_IP)
                                      :argv[7];
  context.vpn_gateway_ip_desired = argc >= 9 ? argv[8] : NULL;

  if(context.vpn_gateway_ip_desired == NULL){
    if((context.vpn_gateway_ip_desired = get_default_gateway_ip()) == NULL)
    {
      fprintf(stderr,"Ooops, poo-poo m... maybe-be-be you don't connect to the internet\n");
      return -3;
    }
  }
  
  if((context.default_interface_name = get_default_interface_name()) == NULL)
  {
      fprintf(stderr,"Ooops, poo-poo m... maybe-be-be you don't connect or not configure internet\n");
      return -3;
  }
  get_tun6_addresses(&context);
  context.tun_fd = tun_create();
  return 0;
}

void get_tun6_addresses(Context* context)
{
  static char local_tun_ip6[40], remote_tun_ip6[40];
  snprintf(local_tun_ip6,sizeof(local_tun_ip6),"64:4f8f::%s",context->local_tun_ip);
  snprintf(remote_tun_ip6,sizeof(remote_tun_ip6),"64:4f8f::%s",context->remote_tun_ip);
  context->local_tun_ip6 = local_tun_ip6;
  context->remote_tun_ip6 = remote_tun_ip6;
}
static char* get_default_gateway_ip()
{
  return exec_shell_command("ip route show default 2> /dev/null | awk '{print $3}'");
}
static char* get_default_interface_name()
{
  return exec_shell_command("ip route show default 2> /dev/null | awk '{print $5}'");
}
int tun_create(char if_name[IFNAMSIZ])
{
  struct ifreq ifr;
  int fd;
  fd = open("/dev/net/tun",O_RDWR);
  if(fd == -1)
  {
    perror("It's over, tun interface not supported");
  }
  ifr.ifr_flags = IFF_TUN | IFF_NO_PI;
  snprintf(ifr.ifr_name, IFNAMSIZ, "");
  if(ioctl(fd, TUNSETIFF, &ifr))
  {
    perror("cannot write to tun device error:");
    return -1;
  }
  snprintf(if_name, IFNAMSIZ, "%s", ifr.ifr_name);
  return fd;
}
