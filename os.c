#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <memory.h>

#define KEY_LENGTH 32

char* exec_shell_command(const char* command)
{
  FILE* fp;
  char gw[64];
  memset(gw,'\0',sizeof(gw));
  if((fp = popen(command, "r")) == NULL)
  {
    return NULL;
  }
  if(fgets(gw, sizeof(gw), fp) == NULL)
  {
    pclose(fp);
    fprintf(stderr,"%s\n",command);
    return NULL;
  }
  pclose(fp);
  return  *gw == '\0' ? NULL : gw;
}


int read_key_file(const char* filename,const unsigned char* key_buffer)
{
  memset(key_buffer,'\0',KEY_LENGTH);

  int file_fd = open(filename, O_RDONLY);
  if(file_fd == -1)
  {
    perror("Error:");
    return -1;
  }
  read(file_fd, key_buffer, KEY_LENGTH);
  
  close(file_fd);
  return 0;
}
